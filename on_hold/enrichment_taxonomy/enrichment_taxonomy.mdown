# The language of data riches: Taxonomy, data quality control, and standard template reporting for enrichment of datasets.

<!-- might want to send this as a combo with the API paper, will depend on how quickly we can get this done -->

Marcelo Oliveira  
Darlan Chrystian  
Keila Santos  
Davi Prates  
Jose Eduardo Santana  
Jacson Barros  
Joao Vissoci  
Elias Carvalho  
Ricardo Pietrobon


## Abstract
<!-- will write at the end --> 

## Introduction




## Methods
### Databases

<!-- use the same datasets and description from the other paper -->

### Taxonomy
<!-- take from the LOD literature and implement a script within mongodb  -->
<!-- need to start searching for existing ontologies. suggested fields:

geolocation
gender
age
family size
income
occupation
education
religion
race
nationality

 -->

### Thesaurus
<!-- add dictionaries so that can automatically search across different databases within online repositories 

for example: gender, male, female, sex, ...

would have to be able to search within data as well as data dictionaries

-->



### Data quality control
<!-- use protocol similar to Katia's  -->


### Standard reporting
<!-- include data quality report, standard table 1, general code for inferential stats and modeling -->

## Results

## Discussion

### Primacy and summary

### Result 1
### Result 2
### Result 3
### Result 4
### Limitations

### Future
